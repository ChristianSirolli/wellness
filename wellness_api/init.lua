if not minetest.settings:get_bool("enable_damage") then
	minetest.log("warning", "[wellness_api] Wellness will not load when damage is disabled.")
	return
end

wellness_api = {
    registered_wellness_types = {},
}

local huds = {}

local function set_player_attr(player, key, value)
	if player.get_meta then
		local meta = player:get_meta()
		if meta and value == nil then
			meta:set_string(key, "")
		elseif meta then
			meta:set_string(key, tostring(value))
		end
	else
		player:set_attribute(key, value)
	end
end

local function get_player_attr(player, key)
	if player.get_meta then
		local meta = player:get_meta()
		return meta and meta:get_string(key) or ""
	else
		return player:get_attribute(key)
	end
end

function wellness_api.register_wellness_type(name, def)
    if wellness_api.registered_wellness_types[name] == nil then
        wellness_api.registered_wellness_types[name] = def
    else
        minetest.log('error', '[wellness_api] '..name..' has already been registed. Skipping.')
    end
end

function wellness_api.override_wellness_type(name, def)
    if wellness_api.registered_wellness_types[name] != nil then
        wellness_api.registered_wellness_types[name] = def
    else
        minetest.log('error', '[wellness_api] '..name..' is not registered. Skipping.')
    end
end

function wellness_api.unregister_wellness_type(name)
    if wellness_api.registered_wellness_types[name] then
        wellness_api.registered_wellness_types[name] = nil
    else
        minetest.log('error', '[wellness_api] '..name..' is not registered. Skipping.')
    end
end

function wellness_api.get_wellness_level(name, player)
    if player.get_meta then
		local meta = player:get_meta()
		return meta and meta:get_string('wellness:'+name) or ""
	else
		return player:get_attribute('wellness:'+name)
	end
end

function wellness_api.set_wellness_level(name, player, level)
    -- Update Metadata
    if player.get_meta then
		local meta = player:get_meta()
		if meta and value == nil then
			meta:set_string(key, "")
		elseif meta then
			meta:set_string(key, tostring(value))
		end
	else
		player:set_attribute(key, value)
	end
    -- Update HUD
    player:hud_change(huds[player:get_player_name()]+'_'+name, "number", math.min(settings.visual_max, level))
end

function wellness_api.incr_wellness_level(name, player, level)
    local curLvl = wellness_api.get_wellness_level(name, player)
    wellness_api.set_wellness_level(name, player, curLvl + level)
end

function wellness_api.decr_wellness_level(name, player, level)
    local curLvl = wellness_api.get_wellness_level(name, player)
    wellness_api.set_wellness_level(name, player, curLvl - level)
end

minetest.register_on_joinplayer(function(player)
    for name, def in pairs(wellness_api.registered_wellness_types) do
        local level = stamina.get_saturation(player) or settings.visual_max
        local id = player:hud_add({
            name = "stamina",
            hud_elem_type = "statbar",
            position = {x = 0.5, y = 1},
            size = {x = 24, y = 24},
            text = "stamina_hud_fg.png",
            number = level,
            text2 = "stamina_hud_bg.png",
            item = settings.visual_max,
            alignment = {x = -1, y = -1},
            offset = {x = -266, y = -110},
            max = 0,
        })
        set_hud_id(player, id)
        stamina.set_saturation(player, level)
        -- reset poisoned
        stamina.set_poisoned(player, false)
        -- remove legacy hud_id from player metadata
        set_player_attribute(player, "stamina:hud_id", nil)
    end
end)

minetest.register_on_leaveplayer(function(player)
	set_hud_id(player, nil)
end)

minetest.register_on_respawnplayer(function(player)
	stamina.update_saturation(player, settings.visual_max)
end)